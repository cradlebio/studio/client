{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "## Folding proteins with Cradle\n",
    "This tutorial will walk you through the simple steps you need to perform to be able to fold proteins using Cradle. The setup is easy, and it should take just a couple of minutes."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "id": "5a10d185",
   "metadata": {},
   "source": [
    "### Install cradlebio"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a90485ff-981e-4b1e-8178-51b3f1dfe7c7",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%pip install cradlebio"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43545872",
   "metadata": {},
   "source": [
    "### Authenticating\n",
    "* Navigate to [https://app.cradle.bio/client-keys](https://app.cradle.bio/client-keys) \n",
    "* Sign in, and click `COPY CREDENTIALS`.\n",
    "* Run the snippet below and paste in the copied text.\n",
    "\n",
    "That's it! You are now ready to fold proteins."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from cradlebio import alphafold, auth\n",
    "\n",
    "creds = auth.authorize()\n",
    "# You can also cache the credentials for subsequent runs, but make sure not to share them by accident!\n",
    "# creds = auth.authorize(safe_creds_file = '/tmp/cradle_credentials.json')\n",
    "\n",
    "client = alphafold.Alphafold(creds)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "<a name=\"quickstart\"></a>\n",
    "## Quickstart\n",
    "\n",
    "The expected format for FASTA files is:\n",
    "```\n",
    ">protein1\n",
    "MTDDK...\n",
    ">protein2\n",
    "KDVLRD...\n",
    "```\n",
    "\n",
    "In case you want to fold multimers write them as follows:\n",
    "```\n",
    ">multimer\n",
    "CHAIN1:CHAIN2:CHAIN3...\n",
    ">homotrimer\n",
    "CHAIN:CHAIN:CHAIN\n",
    "```\n",
    "Note: a FASTA file should contain either only monomers or only multimers."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Upload a FASTA file and run structure prediction:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "filename = ...\n",
    "job = client.predict_from_file(filename)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "To view/save your results skip to: [Fetching the results of your prediction](#fetching-the-results-of-your-prediction)\n",
    "To visualize your folded structures skip to: [Visualizing folded proteins and confidence scores](#visualizing-folded-proteins-and-confidence-scores)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Deep Dive"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "<a name=\"run-structure-prediction\"></a>\n",
    "### Run Structure Prediction\n",
    "\n",
    "**A note on naming**:\n",
    "A unit you can run structure prediction on (and analyze its results) is called a `Structure`. Each structure has one\n",
    "or more amino acid `Sequence`s depending on whether it's a monomer or a multimer. Multiple `Structures` of the same\n",
    "type (monomer or multimer) can be conceptually batched into one `Job`.\n",
    "\n",
    "After starting a structure prediction as described below, the job will be submitted to Cradle's servers. Stopping the cell or closing the browser window will not affect the job that is performing the folding. You can come back in an hour or in a week, your proteins will still be there.\n",
    "\n",
    "**However**, if you choose to display a progress bar (as is set by default), the cell will block until the entire job is completed. If you choose not to show progress, the cell will finish immediately, and you will be able to manually track your job's progress as described in [Monitoring Non-Blocking Jobs](#monitoring-non-blocking-jobs)."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Choose whether to run this tutorial in blocking mode (recommended for starting out):"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "block = True"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "#### From FASTA Files\n",
    "See [Quickstart](#quickstart)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "job = client.predict_from_file(fasta_file='my_fasta_file.fasta',\n",
    "                               show_progress=block,\n",
    "                               relax=False,\n",
    "                               use_cache=False)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "#### Manually\n",
    "##### Single Protein\n",
    "To predict the structure of a single protein, invoke `Alphafold.predict()` with a `Sequence` or a list of `Sequence`s (for multimers). If you call `Alphafold.predict()` with a `str` it will be automatically converted into a `Sequence` object."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from cradlebio.alphafold import Sequence"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "monomer_structure = client.predict('ICISFCQVMVNCDHKHGASV',\n",
    "                                   structure_name='my first monomer',\n",
    "                                   show_progress=block)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "multimer_structure = client.predict(Sequence('NCTRSLGKXMAGNQRNHWGQ', id='ChainA'),\n",
    "                                   Sequence('KVYHVLPXYTICDSHQHFEF', id='ChainB'),\n",
    "                                   structure_name='my first multimer',\n",
    "                                   show_progress=block)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "##### Multiple Proteins\n",
    "You can fold multiple proteins at the same time by creating a `Job` instance and calling `Job.predict` for each protein.\n",
    "As in FASTA files, make sure one `Job` contains only monomers or only multimers:\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "with client.create_job(job_id='demo-manual-job-creation', show_progress=block) as job:\n",
    "    job.predict('AISISGPISRITDDRVTE', name='Transcriptional Repressor IclR')\n",
    "    job.predict('MNTINIAKNDFSDIELAAIP', name='RPOL_BPT7')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "<a name=\"monitoring-non-blocking-jobs\"></a>\n",
    "### Monitoring Non-Blocking Jobs\n",
    "This section is relevant only for non-blocking prediction runs. If you want to see your results jump to To view/save your results skip to: [Fetching the results of your prediction](#fetching_the_results_of_your_prediction)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "<a name=\"monitoring-the-job-status\"></a>\n",
    "#### Monitoring the Job Status\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The `Structures` of a non-blocking `Job` are not guaranteed to be folded. If you don't already have them, you can fetch them by calling `Job.structures()`. All the information about them can be seen by calling `Structure.to_dict()`."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "for structure in job.structures():\n",
    "    print(f'[{structure.to_dict()[\"status\"]}] {structure.name}')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Waiting For Completion\n",
    "\n",
    "Waiting for the entire Job using `Job.wait`"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "folded_structures = tuple(structure_future.result() for structure_future in job.wait().done)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Alternatively, you can wait for a specific structure like so:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "my_structure = ... # some chosen structure\n",
    "folded_structure = my_structure.folded()"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Because we parallelize folding, structures will not finish in any particular order. If you would like to see a folded structure as soon as it's available, run:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from concurrent.futures import as_completed\n",
    "\n",
    "for structure_future in as_completed(s.folded() for s in job.structures()):\n",
    "    folded_structure = structure_future.result()"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Retrieving Old Jobs\n",
    "As mentioned earlier, you don't need to keep your browser tab open until your jobs finish. As long as you have your credentials (see [Get A Credentials Object](#authenticating)) you can come back to look at your results any time. Look for your `Job` in the following ways:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# list running folding jobs by running:\n",
    "print(\"Running:\")\n",
    "for running_job in client.get_jobs():\n",
    "    print(running_job.id)\n",
    "\n",
    "# search jobs for any substring present in the job name (which includes the\n",
    "# name of the original fasta file), status, creation date or even md5 hash.\n",
    "print(\"Done:\")\n",
    "for completed_job in client.search_jobs('DONE', active_only=False):\n",
    "    print(completed_job.id)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "<a name=\"fetching-the-results-of-your-prediction\"></a>\n",
    "## Fetching the results of your prediction\n",
    "\n",
    "The results of a single structure prediction will be wrapped in a `Structure` object. If you don't already have it, you can access the `Structure`s from your `Job` by calling `Job.structures()`.\n",
    "\n",
    "Once you have a folded `Structure`, you can fetch its results by running `Structure.fold_result`. We also provide access to the input alignment for the AlphaFold model, by calling the `Structure.alignment_result` method.\n",
    "\n",
    "> If you are running in non-blocking mode, be sure your `Job` has finished before trying to fetch its results, for instructions on how to do that see [Waiting for completion](#waiting-for-completion)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "for structure_future in as_completed(s.folded() for s in job.structures()):\n",
    "    folded_structure = structure_future.result()\n",
    "    # get the second-best unrelaxed folding result\n",
    "    fold_result = folded_structure.fold_result(relaxed=False, rank=1)\n",
    "\n",
    "    with fold_result.pdb as f:\n",
    "        print(f'Showing results for {folded_structure.name} saved at {fold_result.path}: \\n {f.read()}')\n",
    "\n",
    "    # Alternatively you can save the file locally by running:\n",
    "    # fold_result.to_file(f'{folded_structure.name}.pdb')"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "id": "7352b362",
   "metadata": {},
   "source": [
    "# Visualizing folded proteins and confidence scores\n",
    "The code snippets below visualize the folded structure. To view the structure in more detail, with the plddt scores for each residue superimposed to the structure, please visit [Cradle's WebUI](https://app.cradle.bio/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "%pip install py3Dmol"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# choose the first structure to preview\n",
    "demo_structure = next(job.structures())"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "import py3Dmol\n",
    "from colorsys import hls_to_rgb\n",
    "\n",
    "def colors(n):\n",
    "  def rgb_to_hex(rgb):\n",
    "      rgb = tuple(map(lambda x: round(255 *x), rgb))\n",
    "      return '#' + '%02x%02x%02x' % rgb\n",
    "\n",
    "  return [rgb_to_hex(hls_to_rgb(2/3 * i/n, 0.6, 0.7))\n",
    "          for i in range(1, n + 1)]\n",
    "\n",
    "\n",
    "view = py3Dmol.view(js='https://3dmol.org/build/3Dmol.js',)\n",
    "with demo_structure.fold_result(relaxed=False).pdb as f:\n",
    "    pdb = f.read()\n",
    "    view.addModel(pdb,'pdb')\n",
    "    chain_ids = {l.split()[4] for l in pdb.split('\\n') if l.startswith('ATOM')}\n",
    "\n",
    "sequence_count = len(demo_structure.sequences)\n",
    "for chain_id, color in zip(chain_ids, colors(sequence_count)):\n",
    "    view.setStyle({'chain': chain_id}, {'cartoon': {'color': color}})\n",
    "\n",
    "view.zoomTo()\n",
    "view.show()"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "This code snippet downloads the pae scores from GCS (Google Cloud Storage) and visualizes them using plotly:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "from google.cloud import storage\n",
    "import numpy as np\n",
    "import plotly.express as px\n",
    "\n",
    "# get an access token, create a storage client and a GCS bucket\n",
    "gcs = storage.Client(credentials=creds.as_access_token_credentials())\n",
    "bucket = gcs.bucket(alphafold.CRADLE_GCS_BUCKET)\n",
    "\n",
    "demo_structure = next(job.structures())\n",
    "data = demo_structure.to_dict()\n",
    "extra_data = data['gcs_path'] + '/' + data['extra_data'][0]\n",
    "bucket.blob(extra_data).download_to_filename('/tmp/out')\n",
    "with open('/tmp/out') as f:\n",
    "    scores = json.load(f)\n",
    "    paes = np.array(scores['pae'])\n",
    "\n",
    "px.imshow(paes, width=1200, height=1200)"
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "5203e6db8372b4a1b5aed326f1444ee9ebba23946131c640a554970a0b43db5d"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
