> **Note:** This structure prediction service is currently experimental and accessible only for members of our user community. [Sign up](https://cradlebio.typeform.com/to/lEzf6l1E) if you like to use the service .

Documentation available at https://cradlebio.readthedocs.io/ 

## Known Limitations

### Resources

Our services scale automatically with the number of input sequences you provide.
Unfortunately it also scales our cost linearly. Please don’t try to fold the entirety of UniRef and limit
usage to a single batch of maximum 100 proteins at a time. If you are planning to fold more then 300 proteins at a time, please let us know so we can increase quota.

### Alignments

MMseqs2 is very precise and sensitive, however there are some differences to
the original AlphaFold alignments as outlined in [this paper](https://www.biorxiv.org/content/biorxiv/early/2022/02/08/2021.08.15.456425.full.pdf)

## What’s next?

- Job Management: we will make it easier to see your running jobs in a user interface
- Hosted Notebook: we’re building a hosted notebook to help you get more protein engineering done quicker
- Machine Learning Services: we will start adding more machine learning models to the client library

## License

- [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Acknowledgements

- [DeepMind](https://www.deepmind.com/): we want to thank the AlphaFold team for building this amazing model, and making both the graph and weights available
- [MMseqs2](https://github.com/soedinglab/MMseqs2): we want to thank the MMseqs2 team for building a great sequence searching library

<!-- Links -->

[run-in-colab]: https://colab.research.google.com/drive/1OGlJakWBYSH1lKPZ4mTiWmciqh76i48M
[gitlab-link]: https://gitlab.com/cradlebio/studio/client
[download-ipynb]: https://www.google.com/url?q=https://gitlab.com/cradlebio/studio/client/-/raw/main/demo.ipynb?inline%3Dfalse&sa=D&source=docs&ust=1656749543830560&usg=AOvVaw04z53qY-ZUT-C_ywruI5gM

[colab-logo]: docs/_static/colab.svg
[download-icon]: docs/_static/download.svg
[client-server-diagram]: docs/_static/cradle-flow.jpg