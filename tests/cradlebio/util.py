import os
import uuid

import pytest

from cradlebio import alphafold


def assert_job_not_started(job: alphafold.Job):
    assert not job.started
    assert len(tuple(job.structures())) == 0


def assign_job_id(request: pytest.FixtureRequest) -> str:
    return (request.node.name.replace('[', '_').replace(']', '-')
            + os.getenv('HOSTNAME', f'local')
            + f'-{uuid.uuid4()}')
