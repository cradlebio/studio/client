import concurrent.futures
import datetime

import pytest
from typing import List, Tuple

from google.cloud import firestore

from cradlebio import alphafold
from cradlebio.alphafold import Structure, MsaException, PdbException, STRUCTURES, Sequence
from tests.cradlebio import DATA_PATH
from tests.cradlebio.conftest import recursive_delete
from tests.cradlebio.util import assert_job_not_started
import logging

LOGGER = logging.getLogger(__name__)
TIMEOUT = 2  # todo check before committing
alphafold.JOBS = 'test_jobs'


def _check_future(
        future: concurrent.futures.Future,
        structure_doc: firestore.DocumentReference,
        expecting_failure: bool,
        error_message=None,
        exception_type=None,
):
    if expecting_failure:
        logging.info(f'Expecting failure with {exception_type}')
        with pytest.raises(exception_type, match=error_message):
            future.result(timeout=TIMEOUT)
        return
    assert future.result(timeout=TIMEOUT).id == structure_doc.id


@pytest.mark.parametrize(
    'fold_status,fold_output',
    (('FOLDING_FAILED', False),
     ('DONE', False),
     ('DONE', True)))
@pytest.mark.parametrize(
    'msa_status,msa_output',
    (('MSA_FAILED', False),
     ('MSA_COMPLETE', False),
     ('MSA_COMPLETE', True)))
@pytest.mark.parametrize(
    'sequences',
    (pytest.lazy_fixture('monomer'),
     pytest.lazy_fixture('homodimer'),
     pytest.lazy_fixture('heterodimer'),
     pytest.lazy_fixture('homomultimer'))
)
def test_structure(
        monomer_job: alphafold.Job,
        multimer_job: alphafold.Job,
        msa_status: str,
        msa_output: str,
        fold_status: str,
        fold_output: str,
        sequences: List[Sequence],
):
    if not msa_output and fold_status == 'DONE':
        return

    job = monomer_job if len(sequences) == 1 else multimer_job

    assert_job_not_started(job)
    assert len(tuple(job.structures())) == 0

    with job:
        struct: Structure = job.predict(*sequences, name='test structure')
        assert_job_not_started(job)

    assert job.started
    assert len(tuple(job.structures())) == 1
    assert struct.id == next(job.structures()).id

    struct = next(job.structures())
    assert struct.name == 'test structure'
    aligned_future = struct.aligned()
    folded_future = struct.folded()

    structure_doc = next(job._doc.collection('sequences').stream()).reference
    structure_doc.update({'status': 'MSA_RUNNING'})

    if msa_output or struct.is_multimer:
        structure_doc.update({'a3m': 'I am an a3m path'})
    if struct.is_multimer and msa_output:
        structure_doc.update({'a3m_paired': 'I am a paired a3m path'})
    structure_doc.update({'status': msa_status})

    error_message = None
    if msa_status == 'MSA_FAILED':
        error_message = 'Unknown error during alignment'
    elif not msa_output:
        error_message = 'Missing output'

    expecting_msa_failure = msa_status == 'MSA_FAILED' or not msa_output
    _check_future(
        structure_doc=structure_doc,
        expecting_failure=expecting_msa_failure,
        exception_type=MsaException,
        error_message=error_message,
        future=aligned_future)

    if msa_status == 'MSA_FAILED':
        error_message = 'Unknown error during alignment'
    else:
        if not msa_output:
            error_message = 'Unknown error during folding'
        else:
            if fold_status == 'FOLDING_FAILED':
                error_message = 'Unknown error during folding'
            else:
                if not fold_output:
                    error_message = 'Missing output'
                else:
                    structure_doc.update({'pdbs_unrelaxed': 'I am a path to unrelaxed pdbs'})
                    error_message = None
        structure_doc.update({'status': fold_status})

    _check_future(
        structure_doc=structure_doc,
        expecting_failure=expecting_msa_failure or fold_status == 'FOLDING_FAILED' or not fold_output,
        exception_type=MsaException if msa_status == 'MSA_FAILED' else PdbException,
        error_message=error_message,
        future=folded_future)


def _add_a3m(doc: firestore.DocumentReference, a3m_paired=None, a3m=None):
    if isinstance(doc.get(['seq']).get('seq'), list):
        doc.update({
            'a3m_paired': a3m_paired or 'path to paired a3m file'})
    doc.update({
        'status': 'MSA_COMPLETE',
        'gcs_path': 'fake/gcs/path/',
        'a3m': a3m or 'path/to/a3m-file'})


def _add_pdbs(doc: firestore.DocumentReference, pdbs_unrelaxed=None, pdbs=None):
    doc.update({
        'gcs_path': 'fake/gcs/path/',
        'status': 'DONE',
        'pdbs_unrelaxed': pdbs_unrelaxed or ['path to unrelaxed pdb files'],
        'pdbs': pdbs if pdbs else []})


def test_job(
        multimer_job,
        homodimer: List[Sequence],
        heterodimer: List[Sequence],
        homomultimer: List[Sequence],
):
    job = multimer_job
    test_sequences = {
        'homodimer': homodimer,
        'heterodimer': heterodimer,
        'homomultimer': homomultimer}

    assert_job_not_started(job)
    assert len(tuple(job.structures())) == 0

    structs = []
    with job:
        for name, sequences in test_sequences.items():
            structs.append(job.predict(*sequences, name=name))
        assert_job_not_started(job)

    assert job.started
    assert len(tuple(job.structures())) == len(tuple(job._doc.collection(STRUCTURES).stream())) == 3

    structure_to_update = next(job.structures()).snapshot.reference
    # no structures aligned
    assert len(job.wait(timeout=TIMEOUT, aligned_only=True).done) == 0
    assert len(job.wait(timeout=TIMEOUT).done) == 0

    # just one structure aligned
    _add_a3m(structure_to_update)
    assert len(job.wait(timeout=TIMEOUT, aligned_only=True).done) == 1

    # aligned
    for struct in job.structures():
        _add_a3m(struct.snapshot.reference)
    assert len(job.wait(timeout=TIMEOUT, aligned_only=True).done) == 3

    # no structures completed
    assert len(job.wait(timeout=TIMEOUT).done) == 0

    # just one structure completed
    _add_pdbs(structure_to_update)
    assert len(job.wait(timeout=TIMEOUT).done) == 1

    # completed
    for struct in job.structures():
        _add_pdbs(struct.snapshot.reference)
    assert len(job.wait(timeout=TIMEOUT).done) == 3


def test_duplicated_job(alphafold_client: alphafold.Alphafold):
    original_job = alphafold_client.create_job('duplicate job', show_progress=False)
    duplicate_job = alphafold_client.create_job('duplicate job', show_progress=False)
    try:
        # start original job
        with original_job:
            pass
        with pytest.raises(RuntimeError):
            with duplicate_job:
                pass
    finally:
        recursive_delete(original_job._doc)
        recursive_delete(duplicate_job._doc)


@pytest.mark.parametrize('use_cache', (False, True))
def test_duplicated_job_content(
        alphafold_client: alphafold.Alphafold,
        use_cache: bool):
    original_job = alphafold_client.create_job('original job', show_progress=False)
    duplicate_job = None

    try:
        # start original job
        with original_job:
            pass
        with alphafold_client.create_job('duplicate job', show_progress=False, use_cache=use_cache) as duplicate_job:
            pass

        assert duplicate_job.id == 'original job' if use_cache else 'duplicate job'
    finally:
        recursive_delete(original_job._doc)
        if duplicate_job is not None:
            recursive_delete(duplicate_job._doc)


@pytest.mark.parametrize('job_type', ('monomer', 'multimer'))
def test_duplicated_structure(
        monomer_job: alphafold.Job,
        multimer_job: alphafold.Job,
        monomer: List[Sequence],
        homodimer: List[Sequence],
        job_type: str,
):
    job = multimer_job if job_type == 'multimer' else monomer_job
    structure = homodimer if job_type == 'multimer' else monomer
    with pytest.raises(RuntimeError):
        with multimer_job:
            job.predict(*structure, name='original')
            job.predict(*structure, name='duplicate')


@pytest.mark.parametrize('job_type', ('monomer', 'multimer'))
def test_predict_from_fasta(alphafold_client: alphafold.Alphafold, job_type: str):
    job = alphafold_client.predict_from_file(
        DATA_PATH / job_type / '0.fasta',
        show_progress=False,
        multimer=job_type == 'multimer')
    expected_sequences = {
        'monomer': (('SPIDERMAN',), ('CLARINET',)),
        'multimer': (('SPIDERMAN', 'CLARINET'), ('EXPLAIN', 'PANIC'))
    }

    try:
        for structure, expected_sequences in zip(job.structures(), expected_sequences[job_type]):
            assert tuple(sequence.aas for sequence in structure.sequences) == expected_sequences

    finally:
        if job is not None:
            recursive_delete(job._doc)


def test_predict_from_fasta_duplicate(monomer_job: alphafold.Job):
    with monomer_job, pytest.raises(RuntimeError, match=r'Structure \w* is a duplicate*'):
        alphafold._predict_file(
            DATA_PATH / 'duplicate.fasta',
            job=monomer_job)


@pytest.mark.parametrize('job_type', ('monomer', 'multimer'))
def test_predict_from_directory(alphafold_client: alphafold.Alphafold, job_type: str):
    job = alphafold_client.predict_from_directory(
        DATA_PATH / job_type,
        show_progress=False,
        multimer=job_type == 'multimer')
    expected_sequences = {
        'monomer': {'SPIDERMAN', 'CLARINET', 'EXPLAIN', 'PANIC'},
        'multimer': {
            ('SPIDERMAN', 'CLARINET'),
            ('EXPLAIN', 'PANIC'),
            ('SPIDERMAN', 'SPIDERMAN', 'SPIDERMAN'),
            ('EXPLAIN', 'EXPLAIN'),
            ('QYSTER', 'CLAM'),
            ('FLAW', 'FLAW'),
            ('CAMPER', 'VAN', 'CAMPER', 'VAN', 'VAN')
        }
    }

    try:
        assert {tuple(s.aas for s in structure.sequences) if structure.is_multimer else structure.sequence.aas
                for structure in job.structures()} == expected_sequences[job_type]
    finally:
        if job is not None:
            recursive_delete(job._doc)


@pytest.mark.parametrize('structure_type', ('monomer', 'multimer'))
@pytest.mark.parametrize('job_type', ('monomer', 'multimer'))
def test_mixing_structures(
        request: pytest.FixtureRequest,
        alphafold_client: alphafold.Alphafold,
        monomer: List[Sequence],
        heterodimer: List[Sequence],
        structure_type: str,
        job_type: str,
):
    ok = structure_type == job_type
    structures = {
        'monomer': monomer,
        'multimer': heterodimer,
    }

    def run():
        job = alphafold_client.create_job(
            request.node.name,
            multimer=job_type == 'multimer',
            show_progress=False)
        try:
            with job:
                job.predict(*structures[structure_type])
        finally:
            recursive_delete(job._doc)

    if not ok:
        with pytest.raises(ValueError):
            run()
    else:
        run()


@pytest.mark.parametrize(
    'sequences,alignment_ids',
    ((pytest.lazy_fixture('monomer'), (0,)),
     (pytest.lazy_fixture('homodimer'), (0,)),
     (pytest.lazy_fixture('heterodimer'), (0, 1)),
     (pytest.lazy_fixture('homomultimer'), (0, 1))))
@pytest.mark.parametrize('pair', (False, True))
def test_alignment_result(
        request: pytest.FixtureRequest,
        monomer_job: alphafold.Job,
        multimer_job: alphafold.Job,
        sequences: List[Sequence],
        alignment_ids: Tuple[int],
        pair: bool):
    multimer = len(sequences) > 1
    job = multimer_job if multimer else monomer_job
    try:
        with job:
            structure = job.predict(*sequences, name=request.node.name)

        structure = structure.update()
        a3m = {f'{al_id}.a3m' for al_id in alignment_ids}
        a3m_paired = {f'{al_id}.paired.a3m' for al_id in alignment_ids}

        _add_a3m(
            structure._snapshot.reference,
            a3m=list(a3m),
            a3m_paired=list(a3m_paired) if pair else None)

        aligned_structure = structure.aligned().result(timeout=TIMEOUT)

        result_a3m_paths = {
            aligned_structure.alignment_result(paired=False, sequence_id=sequence.id).path for sequence in sequences}
        expected_a3m_paths = {f'gs://{alphafold.CRADLE_GCS_BUCKET}/fake/gcs/path/{x}' for x in a3m}
        assert result_a3m_paths == expected_a3m_paths

        if not pair:
            return

        if multimer:
            result_a3m_paired_paths = {
                aligned_structure.alignment_result(paired=True, sequence_id=sequence.id).path for sequence in sequences}
            expected_a3m_paired_paths = {f'gs://{alphafold.CRADLE_GCS_BUCKET}/fake/gcs/path/{x}' for x in a3m_paired}
            assert result_a3m_paired_paths == expected_a3m_paired_paths
        else:
            with pytest.raises(ValueError, match='Pair alignment only exists for multimers'):
                aligned_structure.alignment_result(paired=True, sequence_id=sequences[0].id)
    finally:
        recursive_delete(job._doc)


@pytest.mark.parametrize(
    'sequences',
    (pytest.lazy_fixture('monomer'),
     pytest.lazy_fixture('homodimer'),
     pytest.lazy_fixture('heterodimer'),
     pytest.lazy_fixture('homomultimer'),))
@pytest.mark.parametrize('relax', (False, True))
def test_folding_result(
        request: pytest.FixtureRequest,
        monomer_job: alphafold.Job,
        multimer_job: alphafold.Job,
        sequences: List[Sequence],
        relax: bool):
    multimer = len(sequences) > 1
    job = multimer_job if multimer else monomer_job
    try:
        with job:
            structure = job.predict(*sequences, name=request.node.name)

        pdbs_unrelaxed = {f'{i}_unrelaxed.pdb' for i in range(alphafold.ENSEMBLE_LENGTH)}
        pdbs = {f'{i}.pdb' for i in range(alphafold.ENSEMBLE_LENGTH)}

        _add_pdbs(
            structure._snapshot.reference,
            pdbs_unrelaxed=list(pdbs_unrelaxed),
            pdbs=list(pdbs) if relax else None)

        folded_structure = structure.folded().result(timeout=TIMEOUT)

        result_pdb_unrelaxed_paths = {
            folded_structure.fold_result(relaxed=False, rank=i).path for i in range(alphafold.ENSEMBLE_LENGTH)}
        expected_pdb_unrelaxed_paths = {f'gs://{alphafold.CRADLE_GCS_BUCKET}/fake/gcs/path/{x}' for x in pdbs_unrelaxed}
        assert result_pdb_unrelaxed_paths == expected_pdb_unrelaxed_paths

        if relax:
            result_pdb_paths = {
                folded_structure.fold_result(relaxed=True, rank=i).path for i in range(alphafold.ENSEMBLE_LENGTH)}
            expected_pdb_paths = {f'gs://{alphafold.CRADLE_GCS_BUCKET}/fake/gcs/path/{x}' for x in pdbs}
            assert result_pdb_paths == expected_pdb_paths
        else:
            with pytest.raises(
                    ValueError,
                    match=r'Requested relaxed pdb paths from a structure that wasn\'t relaxed.*'
            ):
                folded_structure.fold_result(relaxed=True, rank=0)
    finally:
        recursive_delete(job._doc)


@pytest.mark.parametrize(
    'active_only', (True, False), ids=('active only', 'all'))
def test_get_jobs(
        started_job: alphafold.Job,
        active_jobs: Tuple[firestore.DocumentReference],
        terminated_jobs: Tuple[firestore.DocumentReference],
        alphafold_client: alphafold.Alphafold,
        active_only: bool,
):
    jobs = alphafold_client.get_jobs(active_only=active_only)
    assert len(jobs) >= 1 + len(active_jobs) + (0 if active_only else len(terminated_jobs))

    current_job = [job_in_firestore for job_in_firestore in jobs if job_in_firestore.id == started_job.id]
    assert len(current_job) == 1
    assert next(current_job[0].structures()).name == f'{started_job.id} structure'


def test_get_job_by_id(
        request: pytest.FixtureRequest,
        started_job: alphafold.Job,
        alphafold_client: alphafold.Alphafold):
    job = alphafold_client.get_job_by_id(request.node.name)
    assert job.id == request.node.name


def test_search_job_by_creation_time(
        request: pytest.FixtureRequest,
        started_job: alphafold.Job,
        alphafold_client: alphafold.Alphafold):
    now = datetime.datetime.now()
    jobs = alphafold_client.search_jobs(
        keyword=f'{now.year}-{str(now.month).rjust(2, "0")}-{str(now.day).rjust(2, "0")}')
    assert request.node.name in {job.id for job in jobs}


def test_sequence_too_long(
        request: pytest.FixtureRequest,
        monomer_job: alphafold.Job):
    with pytest.raises(ValueError, match=fr'Structure {request.node.name} is too long, must be shorter than 1400 AAs'):
        with monomer_job:
            monomer_job.predict(Sequence('A' * 1401, id='1401 AAs'), name=request.node.name)


@pytest.mark.parametrize('struct_count', (50, 51))
def test_too_many_structures(
        request: pytest.FixtureRequest,
        monomer_job: alphafold.Job,
        struct_count: int):
    def run():
        with monomer_job:
            for i in range(struct_count):
                monomer_job.predict(Sequence('A' * i, id=str(i)), name=f'{request.node.name}_{i}')

    if struct_count > 50:
        with pytest.raises(ValueError, match=r'Too many structures, a job should contain at maximum 50 structures'):
            run()
    else:
        run()
