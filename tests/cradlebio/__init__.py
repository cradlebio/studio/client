import pkg_resources
from pathlib import Path
import random
import string
from tests.cradlebio import data

DATA_PATH = Path(pkg_resources.resource_filename('tests.cradlebio', 'data'))