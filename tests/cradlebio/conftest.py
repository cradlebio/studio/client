import os.path
from typing import Optional

import pytest
from google.cloud import firestore

from cradlebio import alphafold
from cradlebio.alphafold import Sequence
from cradlebio.auth import IdentityPlatformTokenCredentials
from tests.cradlebio import DATA_PATH

TEST_CREDS_PATH = DATA_PATH / 'test_creds.json'
FIRESTORE_PREFIX = 'test-'


@pytest.fixture(scope='session')
def firebase_creds():
    return IdentityPlatformTokenCredentials.from_file(str(TEST_CREDS_PATH))


def pytest_sessionstart(session):
    if not os.path.exists(TEST_CREDS_PATH):
        print('====================================================================')
        print('Please go to https://app.cradle.bio/client-keys to generate a json credential file and '
              f'copy the downloaded file to {DATA_PATH}/test_creds.json')
        print('====================================================================')
        session.shouldfail = True


def recursive_delete(job_doc: firestore.DocumentReference):
    if job_doc.get().exists:
        sequence_collection = job_doc.collection('sequences')
        for d in sequence_collection.stream():  # users/<user_id>/test_jobs/<job_id>/sequences/<seq_id>
            d.reference.delete()
    job_doc.delete()


@pytest.fixture()
def alphafold_client(firebase_creds: IdentityPlatformTokenCredentials):
    return alphafold.Alphafold(creds=firebase_creds)


def job(request: pytest.FixtureRequest,
        alphafold_client: alphafold.Alphafold,
        job_id: Optional[str] = None,
        multimer: bool = False):
    job_id = job_id or request.node.name
    recursive_delete(alphafold_client.user_doc.collection(alphafold.JOBS).document())

    job = alphafold_client.create_job(job_id, show_progress=False, multimer=multimer)
    try:
        yield job
    finally:
        recursive_delete(job._doc)


@pytest.fixture()
def monomer_job(request: pytest.FixtureRequest,
                alphafold_client: alphafold.Alphafold):
    yield from job(request, alphafold_client, multimer=False)


@pytest.fixture()
def multimer_job(request: pytest.FixtureRequest,
                 alphafold_client: alphafold.Alphafold):
    yield from job(request, alphafold_client, multimer=True)


@pytest.fixture()
def started_job(monomer_job):
    with monomer_job:
        monomer_job.predict(Sequence('HELLQ', id='hello'), name=f'{monomer_job.id} structure')
    assert monomer_job.started
    return monomer_job


def create_job_doc_with_status(
        job_id: str,
        alphafold_client: alphafold.Alphafold,
        status: str,
):
    job_doc = alphafold_client.user_doc.collection(alphafold.JOBS).document(job_id)
    if not job_doc.get().exists:
        job_doc.create({'status': status})
    return job_doc


@pytest.fixture()
def active_jobs(
        request: pytest.FixtureRequest,
        alphafold_client: alphafold.Alphafold):
    statuses = {'PENDING', 'MSA_QUEUE', 'MSA_RUNNING', 'FOLD_QUEUE', 'FOLDING'}
    job_docs = tuple(create_job_doc_with_status(alphafold_client=alphafold_client,
                                                status=status,
                                                job_id=f'{request.node.name}-{status}') for status in statuses)
    try:
        yield job_docs
    finally:
        for job_doc in job_docs:
            recursive_delete(job_doc)


@pytest.fixture()
def terminated_jobs(
        request: pytest.FixtureRequest,
        alphafold_client: alphafold.Alphafold):
    statuses = {'MSA_FAILED', 'FOLDING_FAILED', 'DONE'}
    job_docs = tuple(create_job_doc_with_status(alphafold_client=alphafold_client,
                                                status=status,
                                                job_id=f'{request.node.name}-{status}') for status in statuses)
    try:
        yield job_docs
    finally:
        for job_doc in job_docs:
            recursive_delete(job_doc)


@pytest.fixture()
def monomer():
    return [Sequence('EXPLAIN', id='monomer explain')]


@pytest.fixture()
def monomer_2():
    return [Sequence('STARCH', id='monomer starch')]


@pytest.fixture()
def homodimer():
    return [Sequence('FLAW', id='flaw1'),
            Sequence('FLAW', id='flaw2')]


@pytest.fixture()
def heterodimer():
    return [Sequence('SPIDERMAN', id='spiderman1'),
            Sequence('CHARMING', id='charming1')]


@pytest.fixture()
def homomultimer():
    return [
        Sequence('CLARINET', id='clarinet1'),
        Sequence('PANIC', id='panic1'),
        Sequence('CLARINET', id='clarinet2'),
        Sequence('PANIC', id='panic2'),
        Sequence('PANIC', id='panic3')]


@pytest.fixture()
def monomer_records(monomer, monomer_2):
    return monomer, monomer_2


@pytest.fixture()
def multimer_records(homomultimer, heterodimer, homodimer):
    return homomultimer, heterodimer, homodimer
