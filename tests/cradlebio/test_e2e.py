import logging
from typing import Tuple, Set

from concurrent.futures import as_completed
import pytest

from cradlebio import alphafold
from tests.cradlebio import DATA_PATH
from tests.cradlebio.conftest import recursive_delete
from tests.cradlebio.util import assign_job_id

ALIGNMENT_TIMEOUT_MIN = 5
FOLD_TIMEOUT_MIN = 30


def _test_alignment(job: alphafold.Job, expected_structure_count: int, sequence_ids: Set[Tuple[str]], multimer: bool):
    for structure_future in as_completed((s.aligned() for s in job.structures()), timeout=ALIGNMENT_TIMEOUT_MIN * 60):
        expected_structure_count -= 1
        aligned_structure = structure_future.result(timeout=1)
        assert tuple(s.id for s in aligned_structure.sequences) in sequence_ids

        if multimer:
            for sequence in aligned_structure.sequences:
                with aligned_structure.alignment_result(paired=False, sequence_id=sequence.id).a3m as f:
                    assert f.read() != ''

                with aligned_structure.alignment_result(paired=True, sequence_id=sequence.id).a3m as f:
                    assert f.read() != ''
        else:
            with aligned_structure.alignment_result().a3m as f:
                assert f.read() != ''

    assert expected_structure_count == 0, 'Some structures were not processed'


def _test_fold(job: alphafold.Job, expected_structure_count: int, relax: bool):
    assert len(job.wait(timeout=FOLD_TIMEOUT_MIN * 60, aligned_only=False).done) == len(tuple(job.structures()))
    for structure_future in as_completed(
            (s.folded() for s in job.structures()),
            timeout=FOLD_TIMEOUT_MIN * 60 * expected_structure_count):

        folded_structure = structure_future.result(timeout=1)

        for rank in range(alphafold.ENSEMBLE_LENGTH):
            with folded_structure.fold_result(relaxed=False, rank=rank).pdb as f:
                assert f.read() != ''
            if relax:
                with folded_structure.fold_result(relaxed=True, rank=rank).pdb as f:
                    assert f.read() != ''


@pytest.mark.parametrize(
    'fasta_file, expected_structure_count, chain_ids',
    (('monomer/0.fasta', 2, {('spiderman',), ('clarinet',)}),
     ('multimer/all.fasta', 3, {('heterodimer_0', 'heterodimer_1'),
                                ('homodimer_0', 'homodimer_1'),
                                tuple(f'homomultimer_{i}' for i in range(5))})),
    ids=('monomer', 'multimer'))
@pytest.mark.parametrize(
    'relax', (False, True), ids=('no-relax', 'relax'))
def test_e2e(
        request: pytest.FixtureRequest,
        alphafold_client: alphafold.Alphafold,
        fasta_file: str,
        expected_structure_count: int,
        chain_ids: Set[Tuple[str]],
        relax: bool):
    alphafold.JOBS = 'jobs'
    multimer = fasta_file.startswith('multimer')

    job = None

    try:
        job = alphafold_client.predict_from_file(
            fasta_file=DATA_PATH / fasta_file,
            job_id=assign_job_id(request=request),
            relax=relax,
            show_progress=False,
            multimer=multimer)
        assert job.started

        _test_alignment(
            job=job,
            expected_structure_count=expected_structure_count,
            sequence_ids=chain_ids,
            multimer=multimer)
        logging.info('Passed alignment test')

        _test_fold(job=job, relax=relax, expected_structure_count=expected_structure_count)
        logging.info('Passed fold test')
    finally:
        if job is not None:
            recursive_delete(job._doc)


@pytest.mark.timeout((ALIGNMENT_TIMEOUT_MIN + FOLD_TIMEOUT_MIN) * 60)
def test_golden_path(request: pytest.FixtureRequest, alphafold_client: alphafold.Alphafold):
    structure = None
    try:
        structure = alphafold_client.predict(
            alphafold.Sequence("MDPIRSRTPSPARELLPGPQPDRVQPTADRGGAPPAGGPLDGLPARRTMSRTRLPSPPAP"),
            job_id=assign_job_id(request=request))

        structure.alignment_result().to_file('/tmp/golden.a3m')
        structure.fold_result().to_file('/tmp/golden.pdb')
    finally:
        if structure is not None:
            recursive_delete(structure.job()._doc)
