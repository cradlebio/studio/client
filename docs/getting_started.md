# Getting started

The best way to get started is to follow our tutorial notebook:

| <img src="_static/colab.svg" alt="colab" width="40"/>[Run in Google Colab][run-in-colab] | <img src="_static/download.svg" alt="download" width="24"/>[ Download the notebook][download-ipynb] |
|------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|


## Authenticating with Cradle

You can get the credentials to use our service by signing in to [app.cradle.bio/client-keys](https://app.cradle.bio/client-keys). For now access is restricted to our user community. Any employee of your organization can obtain credentials. If your organization is not part of our user community [please register here](https://cradlebio.typeform.com/to/lEzf6l1E).

You will need to authenticate before starting or accessing jobs. You can do that by triggering an interactive prompt and pasting in the credentials:

```python
from cradlebio import alphafold, auth

credentials = auth.authorize()
client = alphafold.Alphafold(credentials)
```
### Caching credentials

You can also provide a safe location to cache the credentials. In subsequent runs the prompt will not be displayed if the file exists.

Make sure to provide a path in a directory that only you have access to, and it's not easy to share the file by accident. If you're using version control, like git, do not submit the credentials file. On Mac or Linux machines using a path in `/tmp/` might be a reasonable choice.

```python
credentials = auth.authorize(safe_creds_file = '/tmp/cradle_credentials.json')
```

> **⚠️ Important**: Do not share or publish the credentials! Anyone with this file can access all of your uploaded sequences and results. 

### Non-interactive authorization

If you don't want your script to ever be interactive, you can force reading the credentials from a file. 
Same rules as in [Caching credentials](#caching-credentials) apply. 

```
safe_creds_path = '/tmp/cradle_credentials.json'
credentials = auth.IdentityPlatformTokenCredentials.from_file(safe_creds_path)
```
## Quickstart

The expected format for FASTA files is:
```
>protein1
MTDDK...
>protein2
KDVLRD...
```

In case you want to fold multimers write them as follows:
```
>multimer
CHAIN1:CHAIN2:CHAIN3...
>homotrimer
CHAIN:CHAIN:CHAIN
```
Note: a FASTA file should contain either only monomers or only multimers.

Upload a FASTA file and run structure prediction: 
```python
filename = ... 
job = client.predict_from_file(filename)
```

To see how to view/save results skip to: [Fetching the results of your prediction](#fetching-the-results-of-your-prediction)

## Deep dive

### Running structure prediction
**A note on naming**:
A unit you can run structure prediction on (and analyze its results) is called a `Structure`. Each structure has one
or more amino acid `Sequence`s depending on whether it's a monomer or a multimer. Multiple `Structure`s of the same
type (monomer or multimer) can be conceptually batched into one `Job`.

After starting a structure prediction as described below, the job will be submitted to Cradle's servers. Stopping the 
cell or closing the browser window will not affect the job that is performing the folding. You can come back in an 
hour or in a week, your proteins will still be there.

**However**, if you choose to display a progress bar by calling `predict` with `show_progress=True` (as is set by 
default), the cell will block until the entire job is completed. If you choose not to show progress, the cell will 
finish immediately, and you will be able to manually 
track your job's progress as described in [Monitoring Non-Blocking Jobs](#monitoring-non-blocking-jobs). 


#### From FASTA Files
See [Quickstart](#quickstart).

```python
job = client.predict_from_file(fasta_file='my_fasta_file.fasta', relax=False)
```

#### Manually
##### Single Protein
To predict the structure of a single protein, invoke `Alphafold.predict()` with a `Sequence` or a list of `Sequence`s 
(for multimers). If you call `Alphafold.predict()` with a `str` it will be automatically converted into a `Sequence` 
object.

```python
from cradlebio.alphafold import Sequence

monomer_structure = client.predict('ICISFCQVMVNCDHKHGASV',
                                  structure_name='my first monomer')
multimer_structure = client.predict(Sequence('NCTRSLGKXMAGNQRNHWGQ', id='ChainA'),
                                   Sequence('KVYHVLPXYTICDSHQHFEF', id='ChainB'),
                                   structure_name='my first multimer')
```
##### Multiple Proteins
You can fold multiple proteins at the same time by creating a `Job` instance and calling `Job.predict` for each protein. 
As in FASTA files, make sure one `Job` contains only monomers or only multimers:

```python
with client.create_job(job_id='demo-manual-job-creation') as job:
    job.predict(Sequence('AISISGPISRITDDRVTE'), 
                name='Transcriptional Repressor IclR')
    job.predict(Sequence('MNTINIAKNDFSDIELAAIP'), 
                name='RPOL_BPT7')
```
### Monitoring Non-Blocking Jobs

This section is relevant only for non-blocking prediction runs.

#### Monitoring the Job Status

The `Structures` of a non-blocking `Job` are not guaranteed to be folded. 
If you don't already have them, you can fetch them by calling `Job.structures()`. 
All the information about them can be seen by calling `Structure.to_dict()`.

```python
for structure in job.structures():
  print(f'[{structure.to_dict()["status"]}] {structure.name}')
```

#### Waiting For Completion

Waiting for the entire Job using `Job.wait`

```python
folded_structures = tuple(structure_future.result() 
                          for structure_future in job.wait().done)
```

Alternatively, you can wait for a specific structure like so:

```python
my_structure = ... # some chosen structure
folded_structure = my_structure.folded()
```

Because we parallelize folding, structures will not finish in any particular order. If you would like to see a 
folded structure as soon as it’s available, run:
```python
from concurrent.futures import as_completed

for structure_future in as_completed(s.folded() for s in job.structures()):
    folded_structure = structure_future.result()
```

Alternatively, you can use `Future.add_done_callback` to schedule follow-up computation, or `concurrent.futures.as_completed`
to process results as they finish.

See [`concurrent.futures`](https://docs.python.org/3/library/concurrent.futures.html) for details on how to use futures.



### Retrieving Old Jobs
As mentioned earlier, you don't need to keep your browser tab open until your jobs finish. As long as you have your 
credentials (see [Authenticating with Cradle](#authenticating-with-cradle)) you can come back to look at your results 
any time. Look for your `Job` in the following ways:

```python
# list running folding jobs by running:
print("Running:")
for running_job in client.get_jobs():
  print(running_job.id)

# search jobs for any substring present in the job name (which includes the
# name of the original fasta file), status, creation date or even md5 hash.
print("Done:")
for completed_job in client.search_jobs('DONE', active_only=False):
  print(completed_job.id)
```


## Fetching the results of your prediction
The results of a single structure prediction will be wrapped in a `Structure` object. If you don't already have it, you 
can access the `Structure`s from your `Job` by calling `Job.structures()`. 

Once you have a folded `Structure`, you can fetch its results by running `Structure.fold_result`. We also provide 
access to the input alignment for the AlphaFold model, by calling the `Structure.alignment_result` method.

> If you are running in non-blocking mode, be sure your `Job` has finished before trying to fetch its results, for 
> instructions on how to do that see [Waiting for completion](#waiting-for-completion)

```python
for folded_structure in job.structures():
    # get the second-best unrelaxed folding result
    fold_result = folded_structure.fold_result(relaxed=False, rank=1) 

    with fold_result.pdb as f:
        print(f'Showing results for {folded_structure.name} '
              f'saved at {fold_result.path}: \n {f.read()}')

    # Alternatively you can save the file locally by running:
    # fold_result.to_file(f'{folded_structure.name}.pdb')
```

### fsspec.core.OpenFile objects

The objects returned by `AlignmentResult.a3m` and `FoldResult.pdb` are file references to remote files using `fsspec`. 
These references are not serializable (i.e. you will not be able to save them as they are). 
If you would still like to save them, you can do it as follows:

```python
ref = my_structure.aligned().alignment_result().a3m
path: str = ref.fs.unstrip_protocol(ref.path)
```

Then save that string as desired, and load it in another environment where you have access to Cradle credentials:

```python
from cradlebio import auth
import fsspec
import Bio

credentials = auth.authorize()
with auth.fsspec_creds(credentials), fsspec.open(path, 'rt') as f:
   alignment = Bio.AlignIO.parse(f, 'fasta')
```

See the [`fsspec`](https://filesystem-spec.readthedocs.io/en/latest/api.html) docs for more things you can do with
these objects.

## Bugs, Feature Request & Troubleshooting

This is an alpha release of our structure prediction service, 
and it is bound to have some rough edges.
We would love to learn about any bugs you may find or feature 
requests you have. If you prefer not to use Gitlab, you can 
also send us an email at [bugs@cradle.bio](mailto:bugs@cradle.bio).

- [If you find a bug, please file it here](https://gitlab.com/cradlebio/studio/client/-/issues/new)
- [If you have a feature request, please file it here](https://gitlab.com/cradlebio/studio/client/-/issues/new)

[We can help you live troubleshoot an issue by joining our Slack channel. ](https://app.slack.com/client/T03BZQKFXAS/C03BRS991KR)

<!-- Links -->

[run-in-colab]: https://colab.research.google.com/drive/1OGlJakWBYSH1lKPZ4mTiWmciqh76i48M
[gitlab-link]: https://gitlab.com/cradlebio/studio/client
[download-ipynb]: https://www.google.com/url?q=https://gitlab.com/cradlebio/studio/client/-/raw/main/getting_started.ipynb?inline%3Dfalse&sa=D&source=docs&ust=1656749543830560&usg=AOvVaw04z53qY-ZUT-C_ywruI5gM

[colab-logo]: _static/colab.svg
[download-icon]: _static/download.svg
