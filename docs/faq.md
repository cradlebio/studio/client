# FAQ

## Folding

How long does it take to fold a protein?

> Folding a single protein takes about 15 mins for 100AAs. So a protein of length 300, will take 45 minutes to fold, a protein of length 1000 may take 4-5 hours. You can fold up to 10 batches of a 100 proteins per batch with each batch running in parallel. So in this example the maximum throughput would be a 1000 proteins of length 100AAs per 15 minutes.

> However, we currently only support up to 100 parallel folding jobs for all our test users. If you are planning to fold more then 300 proteins at a time, please let us know so we can increase quota.

How long does it take to perform the multiple sequence alignment?

> The time for the multiple sequence alignment is typically orders of magnitude lower than folding.


Can I use `AlphaFold2` in multimer mode? 
>  We don’t currently support this, but are working on adding it soon. Let us know if you would like this feature.

Why are there 5 PDB files for each sequence?
>  AlphaFold uses an ensemble of 5 models and the `pdb()` method returns the result with the highest confidence.
  We do however provide access to each of the 5 results via the `pdbs()` method

Can I predict a structure with a custom template?
>  Not yet, but tell us if this is something you'd like to be able to do.

## Alignments

What databases are used for the `MMSeqs2` alignments? 
>  We currently use `UniRef30` and `Colabfold DB` (based on `BFD/Mgnify`) for `MMSeqs2`.

Can I use a custom multi sequence alignment as an input? 
>  We don’t currently support this. Let us know if you would like this feature.
