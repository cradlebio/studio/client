CradleBio Alphafold
=====================================

Cradle empowers scientists with the world's most advanced machine learning to design proteins & enzymes with fewer,
more successful experiments right from the comfort of their browser. We are building a notebook and set of biological search,
evaluation and optimization models to help you design better proteins.

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: General

   overview.md
   installation.md
   getting_started.md
   privacy_and_security.md
   faq.md
   changelog

.. toctree::
   :maxdepth: 2
   :caption: API Reference

   alphafold
   auth



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
