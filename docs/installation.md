# Installation

Install the Cradle client library using pip:

```bash
 pip install cradlebio
```

Or in notebook environment add this cell:

```bash
%pip install cradlebio
```