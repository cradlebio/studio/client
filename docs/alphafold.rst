Alphafold Module
=========================================================
.. automodule:: cradlebio.alphafold

class ``Structure``
********************************************************
.. autoclass:: cradlebio.alphafold.Structure()
    :members:
    :undoc-members:

class ``Sequence``
********************************************************
.. autoclass:: cradlebio.alphafold.Sequence()


class ``AlignmentResult``
********************************************************
.. autoclass:: cradlebio.alphafold.AlignmentResult()
    :members:

class ``FoldResult``
********************************************************
.. autoclass:: cradlebio.alphafold.FoldResult()
    :members:


class ``Alphafold``
********************************************************
.. autoclass:: cradlebio.alphafold.Alphafold
    :members:


class ``Job``
********************************************************
.. autoclass:: cradlebio.alphafold.Job()
    :members:
    :undoc-members:
