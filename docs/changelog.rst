Changelog
=========

* 2022.07.08 - v0.1.16
    Initial release of the Alphafold Service.
* 2022.08.23 - v0.1.17
    Minor improvements to authentication
* 2022.12.1 - v1.0.0
    Added support for multimers. Refactored client to allow more flexibility in performing actions on the folded
    structures (see docs for more information).


