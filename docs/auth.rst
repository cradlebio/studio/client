Auth Module
============


class ``IdentityPlatformTokenCredentials``
******************************************

.. autoclass:: cradlebio.auth.IdentityPlatformTokenCredentials()
    :members:


class ``WorkloadIdentityCredential``
************************************

.. autoclass:: cradlebio.auth.WorkloadIdentityCredential()
    :members:


function ``authorize``
************************************

.. autofunction:: cradlebio.auth.authorize