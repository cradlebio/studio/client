# Data privacy and security

We understand that by using our services, you’re trusting us with valuable
information. We take this responsibility seriously and work hard to protect your
data. The data you manipulate with Cradle is protected using established
industry standards and we make sure that you are fully in control on how it is
shared.

## Privacy

Data you upload and create with our services is not shared with anyone else.
Cradle’s data storage - including caching mechanisms and ML models - is
partitioned by user account, ensuring that no information is leaked between
users. Your data is not used by Cradle in any other way than to perform the
operations that you requested. Access to your data by authorized Cradle team
members is only permitted on a per-need basis (e.g. to debug a reported issue).
All access is being logged and monitored. If you decide to remove some or all of
your data, the deleted data will be permanently and irrevocably removed.

## Security

We use a variety of industry-standard technologies and services to make sure
your data is protected from unauthorized access, disclosure and use. Our
services are hosted on the Google Cloud Platform, providing the highest level of
physical security. Our python client and - soon - our web client use TLS to
securely transfer the data to and from our servers. Your data is AES 256
encrypted both in transit and at rest.
