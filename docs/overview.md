# Overview

We would love for you to test our first service that provides an easy, fast, secure and scalable way to predict the 3d
structure of a protein using [AlphaFold 2](https://www.nature.com/articles/s41586-021-03819-2)
and [MMseqs2](https://github.com/sokrypton/ColabFold/blob/main/mmseqs.com).


**Isn't AlphaFold already available?**
Yes, AlphaFold is already available in various ways but we have made it even better. We focused on making this service a
joy to use by making it:

- 🧸 **Simple**: it is dead easy to fold proteins, if it's
  not [let us know](https://gitlab.com/cradlebio/studio/client/-/issues/new);
- 🚀 **Fast**: we have not seen a structure prediction service being faster;
- 🔐 **Secure**: all sequences stay private to a user, your foldings are your own;
- ⚖️ **Scalable**: we fold proteins in parallel and take care of runtime resources for you;

## What does the folding service do?

The Cradle folding service provides a dead-easy, fast and scalable AlphaFold structure prediction service.  Cradle provides and manages all the necessary backend infrastructure for alignment, folding, relaxation, etc.

<img src="_static/cradle-flow.jpg" alt="" width=600 />

The client takes protein FASTA files as input and outputs PDBs or the predicted protein structures. 
Since AlphaFold needs a sequence alignment as input we also expose the msa/a3m file. 
