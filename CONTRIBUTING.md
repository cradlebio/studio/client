## Using linters and formatters locally 
### Running linters and formatters locally
On all files:
```bash 
pre-commit run --all-files
```

On a specific file/folder:
```bash 
pre-commit run <file/folder name>
```

More info on pre-commit can be found [here](https://pre-commit.com/).

### Installing pre-commit hooks
To run the linters and formatters at every commit: 

```bash 
pre-commit install
```

In that case if you wish to skip the pre-commit hooks add a `--no-verify` argument to `git commit`:  
```bash 
git commit --no-verify
```